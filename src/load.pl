/*  File:    load.pl
    Purpose: Load my program
*/
encoding(utf8).

:- [andamento_treno_dyn,
    cache,
    downloads,
    regione,
    regione_rules,
    save,
    stazione_dyn,
    stazione_rules,
    treno_dyn,
    treno_rules,
    utils,
    viaggiatreno_api
   ].
