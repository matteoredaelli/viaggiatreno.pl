:- module(utils, [
		  assert_all/1,
		  dump_all/0,
		  list_join/3,
		  save_to_json_file/2
		 ]).

encoding(utf8).

:- use_module(library(http/json)).
:- use_module(library(filesex)).

dump_all:-
    dump([andamento_treno, cache, stazione]).

dump([]).
dump([Atom|List]):-
    dump_atom(Atom),
    dump(List).

dump_atom(Atom):-
    atom_string(Atom,AtomString), string_concat(AtomString,"_dyn.pl", FilenameString), atom_string(Filename, FilenameString),
    tell(Filename), listing(Atom), told.

list_join(List, Separator, String):-
	atomic_list_concat(List, Separator, Atom),
	atom_string(Atom, String).

assert_all([]).
assert_all([Atom|List]):-
%%    retract(Atom),
    assert(Atom),
    assert_all(List).

save_to_json_file(Filename, Data):-
	prolog_to_os_filename(Filename, FilenameOS),
	directory_file_path(Path, _File, FilenameOS),
	make_directory_path(Path),
	setup_call_cleanup(open(FilenameOS, write, Fd),
			   json_write(Fd, Data),
			   close(Fd)).