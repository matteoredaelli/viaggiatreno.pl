treno(CodTreno):- treno(CodTreno, _CodStazione).

treno_regione(CodTreno, CodRegione):-
    treno(CodTreno, CodStazione),
    stazione_regione(CodStazione, CodRegione).
