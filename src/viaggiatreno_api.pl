:- module(viaggiatreno_api,
	  [ andamento_treno/3,
	    arrivi/2,
	    arrivi/3,
	    cache_or_get/1,
	    elenco_stazioni/2,
	    partenze/2,
	    partenze/3
	  ]).

:- use_module(library(http/json)).
:- use_module(library(http/http_open)).
:- use_module(utils).

:- dynamic(cache/1).

url(andamento_treno, "http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/andamentoTreno/~w/~w").
url(partenze, "http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/arrivi/~w/~w").
url(elenco_stazioni, "http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/elencoStazioni/~w").
url(cerca_stazione, "http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/cercaStazione/~w").
url(partenze, "http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/partenze/~w/~w").

%! viaggiatreno_rest(+Url, +Params, -Data) is det
viaggiatreno_rest(Url, Params, Data):-
    url(Url, UrlString),
    format(atom(URL), UrlString, Params),
    uri_iri(URL,URLEncoded),
    %% building filename
    utils:list_join(Params, '_', ParamsString),
    format(atom(Filename), "data/viaggiatreno/~w/~w.json", [Url, ParamsString]),

    %% format(atom(A), '~w/~w', [1000000, "aaa"]), atom_string(A,S).
    setup_call_cleanup(
		       http_open(URLEncoded, In, [
						  %% proxy('proxyzscaler.pirelli.com', 80),
						  request_header('Accept'='application/json'),
						  request_header('Accept-Charset'='utf-8')
						  ]),
		       (set_stream(In, encoding(utf8)),
			json_read_dict(In, Data),
			utils:save_to_json_file(Filename, Data)
		       ),
		       close(In)).


andamento_treno(CodCapolinea, CodTreno, Data):-
    viaggiatreno_rest(andamento_treno, [CodCapolinea, CodTreno], Data).

elenco_stazioni(Regione, Stazioni):-
    viaggiatreno_rest(elenco_stazioni, [Regione], Stazioni).

partenze_arrivi(CodStazione, PartenzeArrivi, Time, Data):-
    stamp_date_time(Time, Date, 'UTC'),
    format_time(atom(TimeString), '%a, %d %b %Y %T GMT', Date, posix),
    viaggiatreno_rest(PartenzeArrivi, [CodStazione, TimeString], Data).

partenze_arrivi(CodStazione, PartenzeArrivi, Data):-
    get_time(Time),
    partenze_arrivi(CodStazione, PartenzeArrivi, Time, Data).

partenze(CodStazione, Data):-
    partenze_arrivi(CodStazione, partenze, Data).
arrivi(CodStazione, Data):-
    partenze_arrivi(CodStazione, arrivi, Data).
partenze(CodStazione, Time, Data):-
    partenze_arrivi(CodStazione, partenze, Time, Data).
arrivi(CodStazione, Time, Data):-
    partenze_arrivi(CodStazione, arrivi, Time, Data).

cache_or_get(X):-
    cache(X) ;
    X,
    assert(cache(X)).
