# viaggiatreno.pl

##  download data

The following commands will save json reponses to a json file under data/viaggiatreno:

- downloads:get_stazioni(1,S), utils:assert_all(S), utils:dump([stazione]).
- catch(downloads:get_andamento_treno("S06039", 684, D), E, writeln("KO")).

## URL utili

 * http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/andamentoTreno/S01645/10882
 * http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/autocompletaStazione/Olgiate
 * http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/cercaNumeroTrenoTrenoAutocomplete/10841
 * http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/cercaStazione/olgiate
 * http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/dettaglioViaggio/S01645/S01520
 * http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/dettaglioStazione/{ID_STAZIONE}/{ID_REGIONE}
 * http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/elencoStazioni/1
 * http://www.viaggiatreno.it/viaggiatrenonew/resteasy/viaggiatreno/news/1/IT

## RIFERIMENTI

 * https://github.com/Razorphyn/Informazioni-Treni-Italiani
 * https://github.com/sabas/trenitalia
 * https://github.com/bluviolin/TrainMonitor
